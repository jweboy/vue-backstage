export { default as AMMenu } from './Menu.vue';
export { default as AMHeader } from './Header.vue';
export { default as AMMain } from './Main.vue';
export { Table, AsyncTable } from '@/components/Table';
